#!/bin/bash

function generate_header(){
    echo '# Dockerfiles

![Docker](https://sqx-bki.fr/wp-content/uploads/2019/11/DockerLogo.png)

There are README within the folders.
' > README.md
}

function generate_catalog(){
    echo '## Catalog

| Project | Content |
|---|---|' >> README.md
    for file in $(find . -type d -maxdepth 1 | sort); do
        if [[ $file != '.' ]] ; then
            if [[ $file != './.git' ]] ; then
                project=$(echo $file | awk -F'/' '{print $2}')
                content=$(grep "Docker content:" $file/README.md | awk -F':' '{print $2}')
                echo "| [$project](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/$project/) | $content |" >> README.md
            fi
        fi
    done
}

function generate_footer(){
    echo '
## Doc generation

For doc generation, please run this command line:

```bash
bash doc-generator.sh
```' >> README.md
}

generate_header
generate_catalog
generate_footer