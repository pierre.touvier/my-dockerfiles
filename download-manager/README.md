# Download Manager (ARIA) dockerfile

Docker content: This repository contains Dockerfile of Aria download manager.

## Aria2 RPC Server + AriaNg Web

### Build

```bash
docker build . -t aria2
```

### Run

```bash
docker run -d --name aria2 \
    -p 6800:6800 \
    -p 127.0.0.1:6880:80 \
    -e PASSWORD=your_password \
    -v /downloads:/aria2/downloads \
    aria2
```

## Only Aria2 RPC Server

### Build

```bash
docker build . -t aria2:server
```

### Run

```bash
docker run -d --name aria2 \
    -p 6800:6800 \
    -e PASSWORD=your_password \
    -v /downloads:/aria2/downloads \
    aria2:server
```

## Only AriaNg Web

### Build

```bash
docker build . -t aria2:client
```

### Run

```bash
docker run -d --name aria2 \
    -p 127.0.0.1:6880:80 \
    aria2:client
```
