# PHP and Apache Dockerfile

Docker content: Base docker image to run PHP applications on Apache

## Building the base image

```bash
    docker build -t apache-php .
```

## Running your Apache+PHP docker image

Start your image binding the external ports 80 in all interfaces to your container:

```bash
    docker run -d -p 80:80 apache-php
```

Test your deployment:

```bash
    open http://localhost/
```

## Enable .htaccess files

If you app uses .htaccess files you need to pass the ALLOW_OVERRIDE environment variable

```bash
    docker run -d -p 80:80 -e ALLOW_OVERRIDE=true apache-php
```

## Loading your custom PHP application

This image can be used as a base image for your PHP application. Create a new `Dockerfile` in your
PHP application folder with the following contents:

```bash
    FROM apache-php
```

After that, build the new `Dockerfile`:

```bash
    docker build -t username/my-php-app .
```

And test it:

```bash
    docker run -d -p 80:80 username/my-php-app
```

Test your deployment:

```bash
    open http://localhost/
```

That's it!

## Loading your custom PHP application with composer requirements

Create a Dockerfile like the following:

```bash
    FROM apache-php
    RUN apt-get update && apt-get install -yq git && rm -rf /var/lib/apt/lists/*
    RUN rm -fr /app
    ADD . /app
    RUN composer install
```

- Replacing `git` with any dependencies that your composer packages might need.
- Add your php application to `/app`
