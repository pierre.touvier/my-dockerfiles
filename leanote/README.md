# Leanote Dockerfile

Docker content: This repository contains dockerfile of Leanote.

## Build the docker

```bash
docker build . -t leanote
```

## Retrieve all needed data

```bash
bash get-data.sh
```

## Instalation

```bash
docker-compose run --rm initdb
```

## Run

```bash
docker-compose up -d leanote
```

exposed on localhost:9000
