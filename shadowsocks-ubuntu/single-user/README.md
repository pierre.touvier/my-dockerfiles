# Docker image of shadowsocks based on ubuntu

> The following part has nothing to do with this warehouse, the following part is the deployment instructions that have been built, you can directly perform the deployment

## Deployment instructions

* Install and test Docker, you can refer to official documents. Or the installation section of "Docker Introduction and Practice".

* Pull the image

```bash
docker pull index.alauda.cn/dubuqingfeng/ubuntu-shadowsocks:vps
```

* Run the container

```bash
docker run -d -p 1984:1984 index.alauda.cn/dubuqingfeng/ubuntu-shadowsocks:vps -p 1984 -k sspassword -m aes-256-cfb
```

* Test run

```bash
docker ps -a
```

If up appears, the installation is successful.

## Configuration instructions

```bash
docker run -d -p 1984:1984 index.alauda.cn/dubuqingfeng/ubuntu-shadowsocks:vps -p 1984 -k sspassword -m aes-256-cfb
```

`1984` is the server port.
`sspassword` is the connection password, change it yourself.
`aes-256-cfb` is the encryption method.

You can also use the `-c /etc/shadowsocks.json` parameter.
