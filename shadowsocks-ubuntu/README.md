# Shadowsocks for Ubuntu Dockerfile

Docker content: This repository contains dockerfile of ubuntu Shadowsocks for single and multi user.

## Single user

[Single User instruction](./single-user/README.md)

## Multi user

[Multi User instruction](./multi-user/README.md)
