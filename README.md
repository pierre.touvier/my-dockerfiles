# Dockerfiles

![Docker](https://sqx-bki.fr/wp-content/uploads/2019/11/DockerLogo.png)

There are README within the folders.

## Retrieve all directories

Some dirs are submodule because I'll not recreate the wheel. In order to pull everything, just run this command:

```bash
git submodule init
git submodule update
```

Or this one:

```bash
git submodule update --remote --merge
```

## Catalog

| Project | Content |
|---|---|
| [amwiki](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/amwiki/) |  This repository contains Dockerfile of Wikimedia. |
| [confluence](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/confluence/) |  This repository contains Dockerfile of Atlassian Confluence. |
| [download-manager](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/download-manager/) |  This repository contains Dockerfile of Aria download manager. |
| [eclipse-che](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/eclipse-che/) |  This repository contains Dockerfile of Eclipse-che. |
| [firefox](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/firefox/) |  This repository contains Dockerfile of Firefox, using X11. |
| [grafana_prometheus](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/grafana_prometheus/) |  This repository contains Dockerfile of Grafana and Prometheus stack. |
| [leanote](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/leanote/) |  This repository contains dockerfile of Leanote. |
| [loki](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/loki/) |  This repository contains dockerfile of loki stack. |
| [lxde-vnc-ubuntu](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/lxde-vnc-ubuntu/) |  This repository contains dockerfile of lxde with VNC. |
| [nextcloud](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/nextcloud/) |  This repository contains dockerfile of Nextcloud. |
| [nginx-ssh](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/nginx-ssh/) |  Simple Ubuntu docker images with SSH access and nginx |
| [pagekit](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/pagekit/) |  This repository contains Dockerfile of PageKit CMS, run with a mysql database. |
| [php-apache](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/php-apache/) |  Base docker image to run PHP applications on Apache |
| [shadowsocks-ubuntu](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/shadowsocks-ubuntu/) |  This repository contains dockerfile of ubuntu Shadowsocks for single and multi user. |
| [shadowsocksr](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/shadowsocksr/) |  the more mainstream solution may be SSR+kcptun. |
| [shlink](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/shlink/) |  |
| [vim](https://gitlab.com/pierre.touvier/my-dockerfiles/-/tree/master/vim/) |  This repository contains dockerfile of Vim. |

## Doc generation

For doc generation, please run this command line:

```bash
bash doc-generator.sh
```
