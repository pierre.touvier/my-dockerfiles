# Shadowsocks Dockerfile

Docker content: the more mainstream solution may be SSR+kcptun.
Basically you can watch 1080P YouTube smoothly. The following describes how to quickly build the server and client (mainly for osx and unix, there are mature GUI solutions on win)

## Server

SSR one-click installation package (quoted from [91yun.org](https://www.91yun.org/archives/2079))

```bash
wget -N --no-check-certificate https://raw.githubusercontent.com/91yun/shadowsocks_install/master/shadowsocksR.sh && bash shadowsocksR.sh
```

### [kcptun](https://github.com/xtaci/kcptun)

`https://github.com/xtaci/kcptun/releases` download the latest version suitable for your system `tar -xf` and decompress it to get server_linux_amd64. Execution server. The excerpts are as follows

```bash
Server: ./server_linux_amd64 -t "127.0.0.1:8388" -l ":4000" -mode fast2 // Forward to the server's local port 8388
Client: ./client_darwin_amd64 -r "Server IP address: 4000" -l ":8388" -mode fast2 // Listen to the local port 8388 of the client
Note: The server needs to have a service to monitor port 8388
```

!!! Very important is the port. It may be that more parameters are filled in. It is easy to be confused if you accidentally fill in it. As a background running program, kcptun can be simple
`nohup command &`

## Use docker

I personally prefer to use docker, think about the advantages may have these two aspects. Docker logs are more convenient to watch logs. Docker's startup items are also more friendly, especially compared to osx's plist. In addition, docker officially supports the lowest version of 3.1. Most of the kernels of openvz are 2.6.32, so if you buy openvz vps, then docker is not available on the server. It is strongly recommended to use docker on the computer client
Quick steps

1. Install docker and docker-compose according to [official document](https://docs.docker.com), set the dockerhub mirror source on this machine, and recommend using the [University of Science and Technology of China](https://docker.mirrors.ustc.edu.cn), [Mirror setting help document](https://lug.ustc.edu.cn/wiki/mirrors/help/docker)
2. git clone this gist to modify the ip and port (modify to your own ip, password and favorite port), you need to modify ssr.json, server_docker_compose.yaml, client_docker_compose.yaml
3. The server is executed in the server_docker_compose.yaml directory

```bash
docker-compose -f server_docker_compose.yaml up --build -d
```

4. The client is executed in the client_docker_compose.yaml directory

```bash
docker-compose -f client_docker_compose.yaml up --build -d
```

In this way, a socks5 proxy of ssr+kcptun is completed

## Extra

1. Socks5 proxy is good, but sometimes we need http proxy. At this time, install privoxy decisively. The simplest setting is as follows

    ```bash
    #osx as an example
    brew install privoxy(brew info privoxy)
    echo -n forward-socks5 / localhost:1083. >> /usr/local/etc/privoxy/config
    # So we have an http proxy with port 8118 (privoxy default), forward to the socks5 proxy we built
    launchctl load ~/Library/LaunchAgents/homebrew.mxcl.privoxy.plist
    ```

2. The http_proxy environment variable in the unix world is very easy to use and reliable. For example, curl, wget, httpie and many command line tools recognize this environment variable. But if you write to death. . . . . Then when you don't need a proxy, you will be sad again, write a shell assistance (switch proxy) sp is to set the proxy sp-to cancel the environment variable

```bash
function sp(){
if [ -n "$1" ];
then
    echo "unset proxy"
    unset {http,https,ftp}_proxy
    unset {HTTP,HTTPS,FTP}_PROXY
else
    echo "set proxy";
    export {http,https,ftp}_proxy="http://127.0.0.1:8118"
    export {HTTP,HTTPS,FTP}_PROXY="http://127.0.0.1:8118"
fi
}
```

3. There is nothing to say about the daily browser using pac to divert. I personally use chrome+SwitchyOmega to cooperate with [this pac rule](https://raw.githubusercontent.com/calfzhou/autoproxy-gfwlist/master/gfwlist.txt)
4. If you want to use safari, set pac or global socket like GUI. You can use the following shell script. ss set pac, ss plus any character set to global socks5, ss c cancel proxy settings. Note, because networksetup requires root privileges. Execute sudo visudo -f /etc/sudoers to add `your_name ALL=(root) NOPASSWD: /usr/sbin/networksetup`

```bash
function ss(){
    case $1 in
        c|C)
        echo "clean proxy"
        sudo networksetup -setautoproxyurl "Wi-Fi" " "
        sudo networksetup -setautoproxystate "Wi-Fi" off
        sudo networksetup -setsocksfirewallproxy "Wi-Fi" "" ""
        sudo networksetup -setsocksfirewallproxystate "Wi-Fi" off
        ;;
        "")
        echo "pac";
        sudo networksetup -setsocksfirewallproxy "Wi-Fi" "" ""
        sudo networksetup -setsocksfirewallproxystate "Wi-Fi" off
        sudo networksetup -setautoproxyurl "Wi-Fi" "http://127.0.0.1:1088/proxy.pac"
        ;;
        *)
        echo "global socks5 proxy"
        sudo networksetup -setautoproxyurl "Wi-Fi" " "
        sudo networksetup -setautoproxystate "Wi-Fi" off
        sudo networksetup -setsocksfirewallproxy "Wi-Fi" '127.0.0.1' '1085'
    esac
}
```

5. kcptun is not particularly mature now, and there may be a sudden interruption of the flow. When you feel impatient, restarting the container may be a good choice -_- `alias dr='docker-compose -f /abs_path /client_docker_compose.yaml restart'`
6. kcptun needs to perform some debugging on different networks to achieve good results. In the worst case, it may appear that the speed of kcptun is worse than using ssr alone. [View discussion: manual parameter setting discussion](https://github.com/xtaci/kcptun/issues/137)
7. If you have a member of Thunderbird, you can verify whether it can accelerate your broadband. If you can, please use [Xunlei-Fastdick](https://github.com/fffonion/Xunlei-Fastdick)`docker run -d --name=xunlei-fastdick --restart=always -e XUNLEI_UID=<uid> -e XUNLEI_PASSWD=<uid> flier/xunlei-fastdick`
8. Win currently does not have a kcptun client, the script refers to [kcptun](http://k162.space/kcptun/)
9. If you want to use safari, set pac or global socket like GUI. You can use the following shell script. ss set pac, ss plus any character set to global socks5, ss c cancel proxy settings.

> Note, because networksetup requires root privileges. Execute sudo visudo -f /etc/sudoers to add `your_name ALL=(root) NOPASSWD: /usr/sbin/networksetup`
